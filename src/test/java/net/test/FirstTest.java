package net.test;

import com.test.actions.Actions;
import com.test.base.BaseTest;
import com.test.pages.Pages;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.test.util.Constants.PRODUCT_NAME;
import static com.test.util.Constants.PRODUCT_PAGE;

public class FirstTest extends BaseTest {

    @BeforeMethod
    public static void setup(){
        Actions.mainActions().openPage(PRODUCT_PAGE);
    }
    @Test
    public void RozetkaTest(){
        Assert.assertEquals(Pages.productPage().getProductName(), PRODUCT_NAME);
        Pages.productPage().clickBuyProduct();
        Assert.assertEquals(Pages.cartPage().getProductName(), PRODUCT_NAME);
        Pages.cartPage().clickResumeBuying();
        Assert.assertEquals(Pages.accessoriesPage().getProductName(), PRODUCT_NAME);
        Pages.header().clickCartButton();
        Assert.assertEquals(Pages.cartPage().getProductName(), PRODUCT_NAME);
        Actions.mainActions().deleteFromCart();
        Assert.assertEquals(Pages.accessoriesPage().getProductName(), PRODUCT_NAME);
        driver.navigate().refresh();
        Pages.header().clickCartButton();
        Assert.assertTrue(Pages.cartPage().isCartEmpty());
    }

}

