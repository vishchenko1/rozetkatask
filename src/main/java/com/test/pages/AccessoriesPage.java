package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class AccessoriesPage extends BasePage {
    private static Locator productName = new XPath("//div[contains(@class, 'detail-title-code pos-fix clearfix')]/h2");

    public String getProductName(){
        waitForElementPresent(productName);
        return getElementText("Getting text from Имя продукта", productName);
    }
}
