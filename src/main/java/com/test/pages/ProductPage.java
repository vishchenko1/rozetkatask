package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class ProductPage extends BasePage {

     //buttons
     private static Locator buyButton = new XPath("//div[contains(@class, 'detail-buy-btn-wrap ng-star-inserted')]//form");
     //fields
     private static Locator productName = new XPath("//div[contains(@class, 'content body-layout ng-star-inserted')]//h1");



     public String getProductName(){
         waitForElementPresent(productName);
         return getElementText("Getting text Имя продукта before buying", productName);
     }

    public void clickBuyProduct(){
        waitForElementPresent(buyButton);
        click("Clicking Купить", buyButton);
    }

}
