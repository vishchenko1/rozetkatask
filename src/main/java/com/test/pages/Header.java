package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class Header extends BasePage {
    private static Locator cartButton = new XPath("//li[contains(@class, 'header-actions__item header-actions__item_type_cart')]");


    public void clickCartButton(){
        click("Clicking Корзина", cartButton);
    }
}
