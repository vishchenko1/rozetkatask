package com.test.pages;

public class Pages {

    private static ProductPage productPage;
    private static CartPage cartPage;
    private static AccessoriesPage accessoriesPage;
    private static Header header;




    public static ProductPage productPage(){
        if(productPage == null){
            productPage = new ProductPage();
        }
        return productPage;
    }

    public static CartPage cartPage(){
        if(cartPage == null){
            cartPage = new CartPage();
        }
        return cartPage;
    }

    public static AccessoriesPage accessoriesPage(){
        if(accessoriesPage == null){
            accessoriesPage = new AccessoriesPage();
        }
        return accessoriesPage;
    }

    public static Header header(){
        if(header == null){
            header = new Header();
        }
        return header;
    }







}
