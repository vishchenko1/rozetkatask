package com.test.pages;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class CartPage extends BasePage {

    //buttons
     private static Locator resumeBuyingButton = new XPath("//div[contains(@class, 'cart-bottom ng-star-inserted')]//a");
     private static Locator removeFromCartButton = new XPath("//div[contains(@class, 'cart-remove')]");
     private static Locator deleteWithoutSavingCartButton = new XPath("//div[contains(@class, 'cart-remove-popup-inner')]/a[1]");
     private static Locator cartCloseButton = new XPath("//a[contains(@class, 'rz-popup-close')]");
    //fields
     private static Locator cartClearField = new XPath("//div[contains(@class, 'cart-dummy-inner h1')]");
     private static Locator productName = new XPath("//div[contains(@class, 'cart-content popup-content ng-star-inserted')]//div[contains(@class, 'purchase-inner clearfix')]/div/a");


    public String getProductName(){
        waitForElementPresent(productName);
        return getElementText("Getting text from Имя продукта inside buying menu", productName);
    }
   public boolean isCartEmpty() {
        return isElementPresent(cartClearField);
    }
    public void clickResumeBuying(){
        waitForElementToBeClickable(resumeBuyingButton);
        click("Clicking Продолжить покупки button", resumeBuyingButton);
    }
    public void clickCloseCartButton(){
        waitForElementToBeClickable(cartCloseButton);
        click("Clicking closing cart", cartCloseButton);
    }
    public void clickRemoveFromCartButton(){
        waitForElementToBeClickable(removeFromCartButton);
        click("Clicking Удалить с корзины", removeFromCartButton);

    }
    public void clickDeleteWithoutSavingButton(){
        waitForElementToBeClickable(deleteWithoutSavingCartButton);
        click("Clicking Удалить без сохранения", deleteWithoutSavingCartButton);
    }
}
