package com.test.util;

public class Constants {
    public static final String DEFAULT_BROWSER = System.getProperty("browser", "chrome");

    public static final int ELEMENT_MEGA_EXTRALONG_TIMEOUT_SECONDS = 360;
    public static final int ELEMENT_EXTRALONG_TIMEOUT_SECONDS = 180;
    public static final int ELEMENT_LONG_TIMEOUT_SECONDS = 120;
    public static final int ELEMENT_TIMEOUT_SECONDS = 60;
    public static final int ELEMENT_SMALL_TIMEOUT_MILLISECONDS = 10;
    public static final int ELEMENT_EXTRASMALL_TIMEOUT_SECONDS = 5;
    public static final int ELEMENT_MICRO_TIMEOUT_SECONDS = 2;

    public static String BASE_URL = System.getProperty("baseurl");
    public static String PRODUCT_NAME = "Apple iPhone X 64GB Silver, Полный комплект (M27)";

    public static final String PRODUCT_PAGE = "https://rozetka.com.ua/99201334/p99201334/";




}
