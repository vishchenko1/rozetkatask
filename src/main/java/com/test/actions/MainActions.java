package com.test.actions;

import com.test.base.BaseActions;
import com.test.pages.Pages;
import org.testng.Reporter;

import static com.test.base.BaseTest.driver;

public class MainActions extends BaseActions {
  /*  public void openMainPage(){
        Reporter.log("Opening main page: " + Constants.ROZETKA_PAGE);
        driver.get(Constants.ROZETKA_PAGE);
    }*/

    public void clearSession() { driver.manage().deleteAllCookies(); }

    public void openPage(String url){
        Reporter.log("Opening page: " + url);
        driver.get(url);
    }

    public void deleteFromCart(){
        Pages.cartPage().clickRemoveFromCartButton();
        Pages.cartPage().clickDeleteWithoutSavingButton();
        Pages.cartPage().clickCloseCartButton();
    }








}
